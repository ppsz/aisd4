#include <iostream>
#include <array>
#include <vector>
#include <chrono>
#include <algorithm>
#include <random>

typedef std::vector<int> int_v;

enum METHOD { BUBBLE, SELECTION, INSERTION, SHELL, MERGE, QUICK };

int number_of_swaps = 0;
int number_of_shifts = 0;

void Swap(int &first, int &second)
{
	number_of_swaps++;
	int temp = first;
	first = second;
	second = temp;
}

void BubbleSort(int_v &tab)
{
	for (int i = 0; i < tab.size(); ++i)
		for (int j = 0; j < tab.size() - 1; ++j)
			if (tab[j] > tab[j + 1])
				Swap(tab[j], tab[j + 1]);
}

void SelectionSort(int_v &tab)
{
	for (int i = 0, k = 0; i < tab.size(); i++)
	{
		k = i;
		for (int j = i + 1; j < tab.size(); j++)
			if (tab[j] < tab[k])
				k = j;
		if (tab[k] != tab[i])
			Swap(tab[k], tab[i]);
	}
}

void InsertionSort(int_v &tab)
{
	int temp, j;
	for (int i = 1; i < tab.size(); ++i)
	{
		temp = tab[i];
		j = i;
		while (j > 0 && tab[j - 1] > temp)
		{
			number_of_swaps++;
			tab[j] = tab[j - 1];
			j--;
		}
		if (tab[j] != temp)
		{
			number_of_shifts++;
			tab[j] = temp;
		}
	}
}

void ShellSort(int_v &tab)
{
	std::array<unsigned long long, 31> power_of_2 = {
		1, 2,  4,  8,  16,  32,  64,
		128,  256,  512,  1024, 2048, 4096, 8192,
		16384, 32768, 65536,131072, 262144, 524288, 1048576,
		2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728,
		268435456, 536870912, 1073741824 };
	int temp, i, j;
	long long int gap=1;

	for (int i = 1; i < 31; ++i)
	{
		if ((tab.size() - 1) / (power_of_2[i]) == 0)
		{
			gap = power_of_2[i - 1] - 1;
			break;
		}
	}

	for (; gap > 0; gap /= 2)
	{
		for (i = gap; i < tab.size(); i++)
			for (j = i - gap; j >= 0 && tab[j] > tab[j + gap]; j -= gap)
				Swap(tab[j], tab[j + gap]);
	}
}

void Merge(int_v &tab, int low, int middle, int high)
{
	int_v temp;
	temp = tab;
	
	int i = low;
	int j = middle + 1;
	int k = low;

	while (i <= middle && j <= high)
	{
		if (temp[i] <= temp[j])
		{
			tab[k] = temp[i];
			++i;
		}
		else
		{
			number_of_swaps++;
			tab[k] = temp[j];
			++j;
		}
		++k;
	}
	while (i <= middle)
	{
		tab[k] = temp[i];
		++k;
		++i;
	}
}

void MergeSort(int_v &tab, int low, int high)
{
	if (low < high)
	{
		int middle = (low + high) / 2;
		MergeSort(tab, low, middle);
		MergeSort(tab, middle + 1, high);
		Merge(tab, low, middle, high);
	}
}

void QuickSort(int_v &tab, int left, int right)
{
	int i = left;
	int j = right;
	int pivot = tab[(left+right)/2];
	do
	{
		while (tab[i] < pivot)
			++i;

		while (tab[j] > pivot)
			--j;

		if (i <= j)
		{
			Swap(tab[i], tab[j]);

			++i;
			--j;
		}
	} while (i <= j);

	if (left < j) 
		QuickSort(tab, left, j);

	if (right > i) 
		QuickSort(tab, i, right);

}

void Sort(int_v &tab, int method)
{
	number_of_swaps = 0;
	auto time1 = std::chrono::high_resolution_clock::now();
	switch (method)
	{
	case BUBBLE:
	{
		BubbleSort(tab);
		break;
	}
	case SELECTION:
	{
		SelectionSort(tab);
		break;
	}
	case INSERTION:
	{
		InsertionSort(tab);
		break;
	}
	case SHELL:
	{
		ShellSort(tab);
		break;
	}
	case MERGE:
	{
		MergeSort(tab, 0, tab.size() - 1);
		break;
	}
	case QUICK:
		QuickSort(tab, 0, tab.size() - 1);
	}
	auto time2 = std::chrono::high_resolution_clock::now();
	std::cout << "Sort took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(time2 - time1).count()
		<< " milliseconds\n";
	if (method == INSERTION)
		std::cout << "Number of shifts: " << number_of_shifts << "\n";
	std::cout << "Number of swaps: " << number_of_swaps << "\n\n";
}

int main()
{
	int max_elements = 1000;
	int_v tab1(max_elements + 1);
	std::random_device rd;
	std::mt19937 mt(rd());

	for (int i = 0; i <= max_elements; ++i)
	{
		tab1[i] = i;
	}
	std::shuffle(tab1.begin(), tab1.end(), mt);

	int_v tab2 = tab1;
	int_v tab3 = tab1;

	//int_v tab = { 44,33,27, 6, 3, 29, 2, 43, 24, 16, 0, 21, 34, 6, 6, 16 };

	//Sort(tab1, BUBBLE);
	//Sort(tab2, SELECTION);
	//Sort(tab3, INSERTION);
	Sort(tab1, SHELL);
	Sort(tab2, MERGE);
	Sort(tab3, QUICK);

	//for (auto element : tab)
	//	std::cout << element << ", ";

	system("pause");
	return 0;
}